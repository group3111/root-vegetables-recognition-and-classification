from django.shortcuts import render
from keras.preprocessing import image
import json
import tensorflow as tf
from tensorflow import Graph
from keras.models import load_model
from keras.preprocessing import image
import numpy as np

# Create your views here.

def index(request):
    return render(request,'index.html')

def result(request):
    return render(request,'result.html')
    
img_height, img_width = 224,224
with open ('./model/class.json','r') as f:
    labelInfo = f.read()
labelInfo = json.loads(labelInfo)

model_graph = Graph()
with model_graph.as_default():
    tf_session = tf.compat.v1.Session()
    with tf_session.as_default():
        model = load_model('./model/modelRootVegetable.h5')

def predictImage(request):
    filepath = request.POST.get('filename','')
    # print("Dopey" ,filepath)
    uploaded_file = request.FILES['image']
    b = uploaded_file.read(1000000000)
    fh = open("image.jpg","wb")
    fh.write(b)
    fh.close()

    img = image.load_img('image.jpg', target_size = (img_height,img_width))
    x = image.img_to_array(img)
    x=x/255
    x = x.reshape(1,img_height,img_width,3)
    with model_graph.as_default():
        with tf_session.as_default():
            predi = model.predict(x)

    predictedLabel = labelInfo[str(np.argmax(predi[0]))]

    context = {'filePathName':filepath, 'predictedLabel':predictedLabel[0]}
    return render(request, 'result.html', context)

def home(request):
    return render(request,'index.html')