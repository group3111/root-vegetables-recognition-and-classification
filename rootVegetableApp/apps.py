from django.apps import AppConfig


class RootvegetableappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rootVegetableApp'
